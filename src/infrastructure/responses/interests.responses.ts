import { ApiProperty } from '@nestjs/swagger';
import { ResponseType } from '../utils/responses.util';

export class InterestResponseModel {
  @ApiProperty()
  public amount: number;
}

export class InterestResponse extends ResponseType {
  @ApiProperty()
  public data: InterestResponseModel;
}
