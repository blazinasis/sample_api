import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { ApplicationModule } from './application/application.module';
import { GlobalExceptionsFilter } from './infrastructure/filters/global.filter';
import { ValidateInputPipe } from './infrastructure/pipes/validation.pipe';
import { MailUtil } from './infrastructure/utils/mailer.util';

/**
 * @function
 * Entry point of the application
 */
async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
  app.useGlobalPipes(new ValidateInputPipe());
  app.useGlobalFilters(new GlobalExceptionsFilter());
  app.enableCors();

  bootstrapDevServerRequirements(app);

  await MailUtil.initialize();

  await app.listen(Number(process.env.PORT), process.env.HOST_NAME);
}

/**
 * @function
 * Bootstraps swagger api documentation
 * @param {INestApplication} app - The nestjs/expressjs application instance
 */
async function bootstrapDevServerRequirements(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Sample Api')
    .setDescription('Sample api documentation')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/v1/docs', app, document);
}

bootstrap();
