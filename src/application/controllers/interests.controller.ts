import { Controller, Get, Query } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { InterestQuery } from 'src/infrastructure/dtos/interests.dto';
import { InterestsService } from '../services/interests.services';
import { InterestResponse } from 'src/infrastructure/responses/interests.responses';

/**
 * @class
 * Controller for interests related api methods.
 * Depends on InterestsService.
 * Has a prefix of `api/v1/interests`
 */
@ApiTags('Interests')
@Controller('api/v1/interests')
export class InterestsController {
  constructor(private readonly interestsService: InterestsService) {}

  /**
   * @method
   * Subscribes an email or refreshes the validation token of an unverified email
   * Is guarded by `UserAlreadySubscribedGuard`
   * @param {UserSubscriptionDto} body - The request body
   * @returns {Promise<UserSubscribeResponse>}`Promise<UserSubscribeResponse>`
   */
  @ApiResponse({ type: InterestResponse })
  @Get()
  public getInterestToBePaid(@Query() body: InterestQuery): Promise<InterestResponse> {
    return this.interestsService.calculateInterestToBePaid(body);
  }
}
