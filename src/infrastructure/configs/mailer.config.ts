/**
 * @object
 * Configuration for nodemailer transport.
 */
export const transportConfig = {
  host: 'smtp.ethereal.email',
  port: 587,
  secure: false,
};
