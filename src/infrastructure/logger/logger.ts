import * as winston from 'winston';
import 'winston-daily-rotate-file';

interface IWinstonLoggerError {
  req: any;
  exception: any;
  methodName?: string;
  className?: string;
  status: number;
}

interface IWinstonLoggerInfo {
  className?: string;
  methodName?: string;
  userId?: string;
  message: any;
  uuid?: string;
}

/**
 * @class
 * Logger Module.
 * Wrapper around winston module.
 */
class WinstonLoggerModule {
  private _winston: typeof winston;
  private _isProduction = false;

  constructor() {
    if ((process.env.NODE_ENV ?? '').toLowerCase() === 'production') {
      this._isProduction = true;
      return;
    }

    winston.add(
      new winston.transports.DailyRotateFile({
        filename: 'logfile.log',
        dirname: process.env.LOG_DIR,
        eol: '\n \n',
      }),
    );

    winston.add(new winston.transports.Console());
    this._winston = winston;
  }

  error(message: IWinstonLoggerError) {
    if (this._isProduction) return;

    const { uuid, ip, ips, method, query, params, headers, path } = message.req;

    this._winston.error({
      Class: `[${message.className ?? 'AllExceptionsFilter'}]`,
      Function: `[${message.methodName ?? 'catch'}]`,
      path: `${method} ${path}`,
      status: message.status ?? 400,
      timestamp: new Date().toUTCString(),
      reqId: uuid,
      message: message.exception,
      metaInfo: {
        ip,
        ips,
        headers,
        query,
        params,
      },
    });
  }

  info(message: IWinstonLoggerInfo) {
    if (this._isProduction) return;
    this._winston.info({
      Class: `[${message.className ?? 'Unknown'}]`,
      Function: `[${message.methodName ?? 'unknown'}]`,
      uuid: message.uuid ?? '',
      timestamp: new Date().toUTCString(),
      message: message.message,
    });
  }
}

/**
 * @instance
 * Logger Module.
 * Wrapper around winston module.
 */
export const Logger = new WinstonLoggerModule();
