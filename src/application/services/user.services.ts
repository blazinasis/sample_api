import { Injectable, InternalServerErrorException } from '@nestjs/common';

import { SubscriptionVerificationQuery } from '../../infrastructure/dtos/user.dto';
import { UserSubscribeResponse, UserSubscribeResponseModel } from '../../infrastructure/responses/user.responses';
import { alphaNumericGenerator } from '../../infrastructure/utils/alphanum.util';
import { MailUtil } from '../../infrastructure/utils/mailer.util';
import { Responses } from '../../infrastructure/utils/responses.util';
import { UserEntity } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';

/**
 * @class
 * Services related to user.
 * Is injected by the framework to dependent controllers.
 */
@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  /**
   * @method
   * Adds a new email to subscribers list. If the email is already subscribed but not
   * verified then updates its validation token.
   * @param email - Email which is either subscribed or updated by creating a new
   * validation token.
   * @returns {Promise<UserSubscribeResponse>} `Promise<UserSubscribeResponse>`
   */
  public async addUserToSubscribers(email: string): Promise<UserSubscribeResponse> {
    const verificationToken = await alphaNumericGenerator();
    const existingUser = await this.userRepository.findOne({ where: { email } });

    await this.userRepository.save({ ...existingUser, email, verificationToken });

    const emailUrl = await MailUtil.sendMail({
      to: email,
      text: `
      Please click the follwing link to verify your subscription.
      ${process.env.VERIFICATION_URL}?token=${verificationToken}&email=${email}
      `,
      html: `
      <b>Please 
          <a href="${process.env.VERIFICATION_URL}?token=${verificationToken}&email=${email}">
                click here
          </a>
       to verify your subscription
      </b>
      `,
    });

    if (!emailUrl) throw new InternalServerErrorException('Error while sending mail');

    return Responses.success<UserSubscribeResponseModel>('Email successfully subscribed', {
      email,
      emailUrl,
    });
  }

  /**
   * @method
   * Verifies the validation token associated with an email and marks the email as verified.
   * @param {SubscriptionVerificationQuery} query - The validation payload containing email and token.
   * @returns {Promise<boolean>} `Promise<boolean>`
   */
  public async verifySubscription(query: SubscriptionVerificationQuery): Promise<boolean> {
    const { email, token } = query;

    const user = await this.userRepository.findOne({ where: { email } });
    if (!user) return false;
    if (user.verificationToken !== token) return false;

    await this.userRepository.save({ ...user, subscribed: true, verificationToken: null });
    return true;
  }

  /**
   * @method
   * Finds an email that is already subscribed i.e. validated.
   * @param email - Email that is to be searched in the db
   * @returns {Promise<UserEntity>} `Promise<UserEntity>`
   */
  public findOneSubscribedByEmail(email: string): Promise<UserEntity> {
    return this.userRepository.findOne({ email, subscribed: true });
  }
}
