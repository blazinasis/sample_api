import { Controller, Get, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { join } from 'path';

import { SubscriptionVerificationQuery } from '../infrastructure/dtos/user.dto';
import { UserService } from './services/user.services';

/**
 * @class
 * Controller class for rendering views used in the application
 * Depends on `UserService`
 */
@Controller()
export class ApplicationController {
  constructor(private readonly userService: UserService) {}

  /**
   * @method
   * Renders landing page
   * @param {Response} response - Express response object
   */
  @Get()
  public renderRootPage(@Res() response: Response): void {
    return response.sendFile(join(__dirname, 'views/index.html'));
  }

  /**
   * @method
   * Verifies user subscription and renders either success or error page based on the result
   * @param {SubscriptionVerificationQuery} query - The subscription verification query
   * @param {Response} response - Express response object
   */
  @Get('/verify')
  public async renderVericationSuccessOrFailedPage(
    @Query() query: SubscriptionVerificationQuery,
    @Res() response: Response,
  ): Promise<void> {
    const result = await this.userService.verifySubscription(query);
    if (result) return response.sendFile(join(__dirname, 'views/success.html'));
    return response.sendFile(join(__dirname, 'views/error.html'));
  }
}
