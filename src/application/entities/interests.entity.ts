import { AfterLoad, Column, Entity } from 'typeorm';

import { InterestsModel } from '../../infrastructure/models/interests.model';
import { EntityBase } from './entity.base';

/**
 * @class
 * @extends {EntityBase}
 * @implements {UserModel}
 * Entity definition for user
 */
@Entity({ name: 'interest_rates' })
export class InterestsEntity extends EntityBase implements InterestsModel {
  @Column('varchar', { name: 'minimum_loan' })
  public minimumLoan: string;

  @Column('varchar', { name: 'maximum_loan' })
  public maximumLoan: string;

  @Column('float', { nullable: true, name: 'interest_rate' })
  public interestRate: number;

  @AfterLoad()
  private convertFromPercentage(): void {
    this.interestRate = this.interestRate / 100;
  }
}
