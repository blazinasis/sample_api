import { CanActivate, ConflictException, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';

import { UserService } from '../services/user.services';

/**
 * @class
 * @implements `CanActivate`
 *
 * A middleware used before user subscription to check if the user is already subscribed.
 */
@Injectable()
export class UserAlreadySubscribedGuard implements CanActivate {
  constructor(private readonly userService: UserService) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  /**
   * @method
   * Checks if the email is already subscribed.
   * @param {Request} request - Express request object.
   * @throws {ConflictException} If user is already subscribed.
   * @return {true} `true` if not.
   */
  async validateRequest(request: Request): Promise<boolean> {
    const userExists = await this.userService.findOneSubscribedByEmail(request.body.email);
    if (userExists) {
      throw new ConflictException('Already subscribed');
    }
    return true;
  }
}
