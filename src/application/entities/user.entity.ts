import { Column, Entity } from 'typeorm';

import { UserModel } from '../../infrastructure/models/user.model';
import { EntityBase } from './entity.base';

/**
 * @class
 * @extends {EntityBase}
 * @implements {UserModel}
 * Entity definition for user
 */
@Entity({ name: 'users' })
export class UserEntity extends EntityBase implements UserModel {
  @Column('varchar', { length: 250, unique: true })
  public email: string;

  @Column('boolean', { default: false })
  public subscribed: boolean;

  @Column('text', { nullable: true, name: 'verification_token' })
  public verificationToken: string;
}
