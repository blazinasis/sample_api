import { ModelBase } from './base.model';

/**
 * @class
 * @extends {ModelBase}
 *
 * Model definition for user that is implemented by `UserEntity`
 */
export class UserModel extends ModelBase {
  public email: string;

  public subscribed: boolean;

  public verificationToken: string;
}
