import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class InterestQuery {
  @IsNotEmpty()
  @ApiProperty()
  public amountToLoan: number;

  @IsNotEmpty()
  @ApiProperty()
  public timePeriod: number;

  @IsNotEmpty()
  @ApiProperty()
  public compound: number;
}
