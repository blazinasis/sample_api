import { ModelBase } from './base.model';

/**
 * @class
 * @extends {ModelBase}
 *
 * Model definition for user that is implemented by `InterestsEntity`
 */
export class InterestsModel extends ModelBase {
  public minimumLoan: string;

  public maximumLoan: string;

  public interestRate: number;
}
