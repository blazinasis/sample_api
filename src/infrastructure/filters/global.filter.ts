import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';

import { Logger } from '../logger/logger';
import { Responses } from '../utils/responses.util';

/**
 * @class
 * @implements {ExceptionFilter}
 *
 * Global exception filter i.e. used to catch exception on a global level.
 * Catches all the uncaught exceptions and handles them.
 */
@Catch()
export class GlobalExceptionsFilter implements ExceptionFilter {
  /**
   * @method
   * Catches the exceptions and handles them
   * @param exception - The exception that ocurred.
   * @param host - Param provided by `Exception filter`
   */
  public catch(exception: unknown, host: ArgumentsHost): void {
    this.handleError(exception, host);
  }

  /**
   * @method
   * Logs the exception that ocurred and returns a response according to the exception
   * @param exception
   * @param host
   * @returns {Response<any, Record<string, any>>} `Response<any, Record<string, any>>`
   */
  private handleError(exception: any, host: ArgumentsHost): Response<any, Record<string, any>> {
    const context = host.switchToHttp();
    const req = context.getRequest() as Request;
    const res = context.getResponse() as Response;

    const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    Logger.error({
      exception: exception?.message ?? exception,
      req,
      status,
      className: 'GlobalExceptionsFilter',
      methodName: 'handleError',
    });

    const error = Responses.error(exception?.message ?? 'Internal server error!');
    return res.status(status).json(error);
  }
}
