import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';
import { config } from 'dotenv';

config();

/**
 * @object
 * The database configuration object.
 */
const dbConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  entities: [join(__dirname, '../../application/entities/*.entity.{js,ts}')],
  migrations: [join(__dirname, '../../migrations/*.{js,ts}')],
  synchronize: false,
  cli: {
    entitiesDir: 'src/application/entities',
    migrationsDir: 'src/migrations',
  },
};

export default dbConfig;
