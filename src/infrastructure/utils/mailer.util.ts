import { createTestAccount, createTransport, getTestMessageUrl, TestAccount, Transporter } from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

import { transportConfig } from '../configs/mailer.config';

/**
 * @class
 * Handles all mail related tasks
 */
class Mailer {
  private _transporter: Transporter;
  private _testAccount: TestAccount;

  /**
   * @method
   * Initializes test mail account and mail transporter.
   * Must be called once the app starts.
   */
  public async initialize(): Promise<void> {
    this._testAccount = await createTestAccount();

    // create reusable transporter object using the default SMTP transport
    this._transporter = createTransport({
      ...transportConfig,
      auth: {
        user: this._testAccount.user,
        pass: this._testAccount.pass,
      },
    });
  }

  /**
   * @method
   * Handles mail sending
   * @param mailOptions - Options for sending mail
   * @returns `String` - The test mail url
   */
  public async sendMail(mailOptions: Mail.Options = defaultMailOptions) {
    const response = await this._transporter.sendMail({
      ...mailOptions,
      from: `"Ashish Kafle" <${this._testAccount.user}>`,
    });

    return getTestMessageUrl(response);
  }
}

const defaultMailOptions: Mail.Options = {
  from: '"Ashish Kafle" <ashishkafle@example.com>',
  to: 'blazinasis@gmail.com',
  subject: 'Confirm Subscription',
  text: 'Please click the follwing link to verify your subscription',
  html: '<b>Hello world?</b>',
};

export const MailUtil = new Mailer();
