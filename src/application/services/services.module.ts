import { Module } from '@nestjs/common';
import { RepositoryModule } from '../repositories/repository.module';
import { InterestsService } from './interests.services';
import { UserService } from './user.services';

/**
 * @module
 * Module that exports all the application services.
 * Depends on `RepositoryModule`
 */
@Module({
  providers: [UserService, InterestsService],
  exports: [UserService, InterestsService],
  imports: [RepositoryModule],
})
export class ServicesModule {}
