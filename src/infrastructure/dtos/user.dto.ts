import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MaxLength } from 'class-validator';

/**
 * @class
 * Data Transfer Object used during user subscription.
 * Validates the data.
 */
export class UserSubscriptionDto {
  @ApiProperty({ description: 'Email of user' })
  @MaxLength(250)
  @IsEmail()
  public email: string;
}

/**
 * @class
 * Query object used during user subscription verification.
 * Validates the query as well.
 */
export class SubscriptionVerificationQuery extends UserSubscriptionDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  public token: string;
}
