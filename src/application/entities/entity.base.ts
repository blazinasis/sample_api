import { CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

/**
 * @abstract @class
 *  * The base entity that is extended by all other entities.
 */
export abstract class EntityBase {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @UpdateDateColumn({ name: 'updated_at' })
  public updatedAt: Date;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  public deletedAt: Date;
}
