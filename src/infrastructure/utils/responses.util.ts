import { ApiProperty } from '@nestjs/swagger';

export enum ResponseStatus {
  success = 'success',
  failed = 'failed',
  errored = 'errored',
}

/**
 * @class
 * Response model definition that is to be used by all RESTful routes.
 * This class is usually extended and @property {data} is overridden.
 */
export class ResponseType<T = any> {
  @ApiProperty({ enum: ResponseStatus })
  status: ResponseStatus;
  @ApiProperty()
  message: string;
  data: T;
}

/**
 * @class
 * A helper class to properly manage and format application responses
 */
class ApplicationResponses {
  public error(message: string, data = []): ResponseType {
    return { data, message, status: ResponseStatus.errored };
  }

  public success<T = any>(message: string, data: T = null): ResponseType {
    return { data, message, status: ResponseStatus.success };
  }

  public failed(message: string, data = []): ResponseType {
    return { data, message, status: ResponseStatus.failed };
  }
}

export const Responses = new ApplicationResponses();
