import { EntityRepository, Repository } from 'typeorm';
import { InterestsEntity } from '../entities/interests.entity';

/**
 * @class
 * @extends {Repository<InterestsEntity>}
 * Repository of `InterestsEntity`.
 * Manages all the db operations of `InterestsEntity`
 */
@EntityRepository(InterestsEntity)
export class InterestsRepository extends Repository<InterestsEntity> {}
