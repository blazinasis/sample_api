import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';

/**
 * @class

 * Transforms and validates request body using DTO classes and transforms if valid
 * otherwise @throws {BadRequestException}
 */
@Injectable()
export class ValidateInputPipe extends ValidationPipe {
  public async transform(value: unknown, metadata: ArgumentMetadata): Promise<any> {
    try {
      return await super.transform(value, metadata);
    } catch (e) {
      if (e instanceof BadRequestException) {
        throw new UnprocessableEntityException({
          error: 'Invalid parameters',
          ...e,
        });
      }
      throw { error: e };
    }
  }
}
