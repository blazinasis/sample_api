import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserSubscriptionDto } from '../../infrastructure/dtos/user.dto';
import { UserAlreadySubscribedGuard } from '../guards/subscribed.guard';
import { UserSubscribeResponse } from '../../infrastructure/responses/user.responses';
import { UserService } from '../services/user.services';

/**
 * @class
 * Controller for user related api methods.
 * Depends on UserService.
 * Has a prefix of `api/v1/users`
 */
@ApiTags('Users')
@Controller('api/v1/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  /**
   * @method
   * Subscribes an email or refreshes the validation token of an unverified email
   * Is guarded by `UserAlreadySubscribedGuard`
   * @param {UserSubscriptionDto} body - The request body
   * @returns {Promise<UserSubscribeResponse>}`Promise<UserSubscribeResponse>`
   */
  @UseGuards(UserAlreadySubscribedGuard)
  @Post()
  public subscribe(@Body() body: UserSubscriptionDto): Promise<UserSubscribeResponse> {
    return this.userService.addUserToSubscribers(body.email);
  }
}
