import { Module } from '@nestjs/common';
import { ServicesModule } from '../services/services.module';
import { InterestsController } from './interests.controller';
import { UserController } from './user.controller';

/**
 * @module
 * Root module for exporting all the controllers of the application
 * Imports ServicesModule
 */
@Module({ controllers: [UserController, InterestsController], imports: [ServicesModule] })
export class ControllerModule {}
