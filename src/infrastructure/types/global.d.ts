declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production';
      PORT: string;
      DB: 'postgres';
      DB_HOST: string;
      DB_PORT: string;
      DB_USER: string;
      DB_PASS: string;
      DB_NAME: string;
      LOG_DIR: string;
      VERIFICATION_URL: string;
      HOST_NAME: string;
    }
  }
}

export {};
