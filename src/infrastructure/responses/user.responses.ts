import { ApiProperty } from '@nestjs/swagger';

import { ResponseType } from '../utils/responses.util';

/**
 * @class
 * The response data type after user is subscribed.
 * Used by swagger to show schema definition.
 */
export class UserSubscribeResponseModel {
  @ApiProperty()
  email: string;
  @ApiProperty()
  emailUrl: string;
}

/**
 * @class
 * The response type after user is subscribed.
 * Used by swagger to show schema definition.
 */
export class UserSubscribeResponse extends ResponseType {
  @ApiProperty()
  data: UserSubscribeResponseModel;
}

/**
 * @class
 * The response type after user subscription is verified
 * Used by swagger to show schema definition.
 */
export class SubscriptionVerificationResponse extends ResponseType {}
