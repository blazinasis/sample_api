import { Injectable } from '@nestjs/common';
import { InterestQuery } from 'src/infrastructure/dtos/interests.dto';
import { Logger } from 'src/infrastructure/logger/logger';
import { InterestResponse } from 'src/infrastructure/responses/interests.responses';
import { Responses } from 'src/infrastructure/utils/responses.util';
import { LessThanOrEqual, MoreThan } from 'typeorm';
import { InterestsRepository } from '../repositories/interests.repository';

/**
 * @class
 * Services related to interests.
 * Is injected by the framework to dependent controllers.
 */
@Injectable()
export class InterestsService {
  constructor(private readonly interestsRepository: InterestsRepository) {}

  /**
   * @method
   * Finds interest for the amount given.
   * @param amount - The amount for which interest is to be calculated
   * @returns {Promise<InterestResponse>}
   */
  public async calculateInterestToBePaid(query: InterestQuery): Promise<InterestResponse> {
    const { amountToLoan, compound, timePeriod } = query;
    const rateForGivenAmount = await this.interestsRepository.findOne({
      where: { maximumLoan: MoreThan(amountToLoan), minimumLoan: LessThanOrEqual(amountToLoan) },
    });

    Logger.info({ message: `for ${amountToLoan} rate is ${rateForGivenAmount.interestRate}` });

    const amount = amountToLoan * (1 + rateForGivenAmount.interestRate / compound) ** (compound * timePeriod);

    return Responses.success('Calculated interest amount', { amount });
  }
}
