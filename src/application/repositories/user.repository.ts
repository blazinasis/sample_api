import { EntityRepository, Repository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';

/**
 * @class
 * @extends {Repository<UserEntity>}
 * Repository of `UserEntity`.
 * Manages all the db operations of `UserEntity`
 */
@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {}
