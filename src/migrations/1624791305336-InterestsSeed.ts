import { MigrationInterface, QueryRunner } from 'typeorm';

export class InterestsSeed1624791305336 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(
      'INSERT into "interest_rates" ("minimum_loan", "maximum_loan", "interest_rate") VALUES (1000, 20000, 5), (20000, 40000, 6), (40000, 1000000000, 7)',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query('DELETE FROM "interest_rates"');
  }
}
