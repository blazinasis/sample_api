import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InterestsRepository } from './interests.repository';
import { UserRepository } from './user.repository';

/**
 * @module
 * Exports all the application data repositories
 */
@Module({ imports: [TypeOrmModule.forFeature([UserRepository, InterestsRepository])], exports: [TypeOrmModule] })
export class RepositoryModule {}
