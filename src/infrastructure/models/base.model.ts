/**
 * @class
 * Base model that is extended by all other models
 */
export class ModelBase {
  public id: number;

  public updatedAt: Date;

  public createdAt: Date;

  public deletedAt: Date;
}
