import { randomBytes } from 'crypto';

/**
 * @function
 * Alphanumeric generator hepler function
 * @returns `String`
 */
export function alphaNumericGenerator(): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      const alphanum = randomBytes(32).toString('hex');
      resolve(alphanum);
    } catch (error) {
      reject();
    }
  });
}
