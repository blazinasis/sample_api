import {MigrationInterface, QueryRunner} from "typeorm";

export class InterestsEntity1624791273001 implements MigrationInterface {
    name = 'InterestsEntity1624791273001'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "interest_rates" ("id" SERIAL NOT NULL, "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "minimum_loan" character varying NOT NULL, "maximum_loan" character varying NOT NULL, "interest_rate" double precision, CONSTRAINT "PK_ab8aa0e606d1113a17f5a3b99ca" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "interest_rates"`);
    }

}
