import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import dbConfig from '../infrastructure/configs/db.config';
import { ApplicationController } from './application.controller';
import { ControllerModule } from './controllers/controller.module';
import { ServicesModule } from './services/services.module';

/**
 * @module
 * The root module that imports all the application dependencies
 */
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.development.local', '.env'],
    }),
    TypeOrmModule.forRoot(dbConfig),
    ControllerModule,
    ServicesModule,
  ],
  controllers: [ApplicationController],
})
export class ApplicationModule {}
